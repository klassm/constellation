Constellation
=============

Constellation is a simulation and project tracking tool, making use of Python
scripts to generate metadata and diagnostic information for projects under
the supervision of Constellation.
